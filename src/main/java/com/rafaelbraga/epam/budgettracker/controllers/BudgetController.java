package com.rafaelbraga.epam.budgettracker.controllers;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.rafaelbraga.epam.budgettracker.models.Budget;
import com.rafaelbraga.epam.budgettracker.models.ResponseWIthNumberAndTime;
import com.rafaelbraga.epam.budgettracker.services.BudgetService;

import io.swagger.annotations.Api;

/**
 * This is simply a specialization of the @Component class and allows
 * implementation classes to be autodetected through the classpath
 * scanning. @Controller is typically used in combination with a @RequestMapping
 * annotation used on request handling methods.
 * 
 * @author Rafael_Braga
 *
 */

@RestController
@RequestMapping
@Api
public class BudgetController {

	@Autowired
	private BudgetService budgetService;

	@PostMapping(value = "budget")
	public ResponseEntity<Budget> create(@Valid @RequestBody Budget budget) {
		Budget obj = budgetService.save(budget);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/id").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).body(obj);
	}
	
	@GetMapping(value = "budgets")
	public List<Budget> findAll() {
		return budgetService.findAll();
	}

	@GetMapping(value = "budgets/total", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseWIthNumberAndTime sumAndTime() {
		return new ResponseWIthNumberAndTime(budgetService.getAllSpendingsSum(), new Date(System.currentTimeMillis()));
	}

}
