package com.rafaelbraga.epam.budgettracker.services;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rafaelbraga.epam.budgettracker.models.Budget;
import com.rafaelbraga.epam.budgettracker.models.ResponseWIthNumberAndTime;
import com.rafaelbraga.epam.budgettracker.repositories.BudgetRepository;

/**
 * Spring @Service annotation is used with classes that provide some business
 * functionalities. Spring context will autodetect these classes when
 * annotation-based configuration and classpath scanning is used.
 * 
 * @author Rafael_Braga
 *
 */

@Service
public class BudgetService {

	@Autowired
	private BudgetRepository budgetRepository;

	public List<Budget> findAll() {
		return budgetRepository.findAll();
	}

	public Budget save(Budget budget) {
		return budgetRepository.save(budget);
	}

	public BigDecimal getAllSpendingsSum() {
		if (budgetRepository.totalSpendings() == null) {
			return BigDecimal.valueOf(0.00);
		} else {
			return budgetRepository.totalSpendings();
		}
	}
}
