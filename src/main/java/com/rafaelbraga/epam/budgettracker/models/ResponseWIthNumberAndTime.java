package com.rafaelbraga.epam.budgettracker.models;

import java.math.BigDecimal;
import java.util.Date;

public class ResponseWIthNumberAndTime {
	private BigDecimal spendings;
	private Date timestamp;
	
	
	public ResponseWIthNumberAndTime(BigDecimal spendings, Date timestamp) {
		this.spendings = spendings;
		this.timestamp = timestamp;
	}
	
	public ResponseWIthNumberAndTime() {
		
	}

	public BigDecimal getSpendings() {
		return spendings;
	}
	
	public void setSpendings(BigDecimal spendings) {
		this.spendings = spendings;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
}
