package com.rafaelbraga.epam.budgettracker.models;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.format.annotation.NumberFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This class represents my Budget model
 * 
 * @author Rafael_Braga
 *
 */

@Entity
public class Budget {

	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Description must not be empty/blank")
	@Column(name = "description")
	private String description;

	@NotNull(message = "Spendings must not be empty/blank")
	@NumberFormat(pattern = "#,###,###,###.##")
	@Column(name = "price", columnDefinition="Decimal(10,2) default '0.00'")
	@Positive(message = "Spendings can not be negative or zero!")
	private BigDecimal price;
	
	public Budget(Long id, String description, BigDecimal price) {
		this.id = id;
		this.description = description;
		this.price = price;
	}
	
	public Budget() {
		
	}

	public Long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
