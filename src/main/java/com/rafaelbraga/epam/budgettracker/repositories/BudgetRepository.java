package com.rafaelbraga.epam.budgettracker.repositories;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.rafaelbraga.epam.budgettracker.models.Budget;

/**
 * With Spring Data, we define a repository interface for each domain entity in
 * the application. A repository contains methods for performing CRUD
 * operations, sorting and paginating data. @Repository is a marker annotation,
 * which indicates that the underlying interface is a repository
 * 
 * @author Rafael_Braga
 *
 */
@Repository
public interface BudgetRepository extends JpaRepository<Budget, Long> {

	/**
	 * @Query annotation from jpa reposiory that we can use to execute SQL
	 *        functions, such as SUM.
	 * @return the sum of all my spendings
	 */
	@Query("SELECT SUM(b.price) FROM Budget b")
	BigDecimal totalSpendings();
}
