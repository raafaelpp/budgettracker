package com.rafaelbraga.epam.budgettracker.integration.spring.tests;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.rafaelbraga.epam.budgettracker.controllers.BudgetController;
import com.rafaelbraga.epam.budgettracker.pojo.Spending;
import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.BeforeStage;
import com.tngtech.jgiven.annotation.Quoted;
import com.tngtech.jgiven.annotation.Table;
import com.tngtech.jgiven.integration.spring.JGivenStage;

@JGivenStage
public class BudgetStages extends Stage<BudgetStages> {

	MockMvc mockMvc;

	@Autowired
	BudgetController budgetController;

	private ResultActions mvcResult;

	@BeforeStage
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(budgetController).build();
	}

	public BudgetStages i_get_spendings_summary(@Quoted String path) throws Exception {
		mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(path).accept(MediaType.APPLICATION_JSON));
		return this;
	}

	public BudgetStages the_status_is(HttpStatus status) throws Exception {
		mvcResult.andExpect(status().is(status.value()));
		return this;
	}

	public BudgetStages the_content_is(@Quoted String content) throws Exception {
		mvcResult.andExpect(content().string(equalTo(content)));
		return this;
	}

	public BudgetStages i_post_a_new_spending(String content) {
		
		try {
			mvcResult = mockMvc.perform(
					MockMvcRequestBuilders.post("/budget").contentType(MediaType.APPLICATION_JSON).content(content));
		} catch (IllegalArgumentException e) {
			
		} catch (Exception e) {
			System.out.println(e);
		}
		return this;
	}

	public BudgetStages the_summary_list_size_is(int size) throws Exception {
		mvcResult.andExpect(jsonPath("$", hasSize(size)));
		return this;
	}

	public BudgetStages that_i_have_the_following_spendings(@Table Spending ...spendings) {
		return this;
	}

	public BudgetStages i_get_total_spendings(@Quoted String path) throws Exception {
		mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(path).accept(MediaType.APPLICATION_JSON));
		return this;
	}

	public BudgetStages the_content_contains(String content) throws Exception {
		mvcResult.andExpect(content().string(Matchers.containsString((content))));
		return this;
	}

}
