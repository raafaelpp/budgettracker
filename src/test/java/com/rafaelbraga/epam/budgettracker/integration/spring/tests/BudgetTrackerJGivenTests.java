package com.rafaelbraga.epam.budgettracker.integration.spring.tests;

import java.math.BigDecimal;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;

import com.rafaelbraga.epam.budgettracker.BudgettrackerApplication;
import com.rafaelbraga.epam.budgettracker.config.H2TestProfileJPAConfig;
import com.rafaelbraga.epam.budgettracker.jgiven.config.BudgetJGivenConfiguration;
import com.rafaelbraga.epam.budgettracker.pojo.Spending;
import com.tngtech.jgiven.annotation.JGivenConfiguration;
import com.tngtech.jgiven.integration.spring.SimpleSpringRuleScenarioTest;

@SpringBootTest(classes = {MockServletContext.class, BudgettrackerApplication.class, H2TestProfileJPAConfig.class})
@WebAppConfiguration
@JGivenConfiguration(BudgetJGivenConfiguration.class)
@ActiveProfiles("test")
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class BudgetTrackerJGivenTests extends SimpleSpringRuleScenarioTest<BudgetStages>{
	
	@Test
	public void validate_that_when_we_get_summary_with_no_spendings_then_we_will_get_an_empty_response() throws Exception {
		when().i_get_spendings_summary("/budgets");
		then().the_status_is(HttpStatus.OK)
		.and().the_content_is("[]");
	}
	
	@Test
	public void validate_that_when_we_post_2_spendingd_then_when_we_get_summary_it_will_contain_only_2_spendings() throws Exception {
		given().that_i_have_the_following_spendings(
				new Spending("Spending 1", BigDecimal.valueOf(10)),
				new Spending("Spending 2", BigDecimal.valueOf(20)))
		.when().i_post_a_new_spending("{\"description\": \"Spending 1\", \"price\":\"10\"}")
		.and().i_post_a_new_spending("{\"description\": \"Spending 2\", \"price\":\"20\"}")
		.when().i_get_spendings_summary("/budgets")
		.then().the_status_is(HttpStatus.OK)
		.and().the_summary_list_size_is(2)
		.and().the_content_is("[{\"description\":\"Spending 1\",\"price\":10.00},{\"description\":\"Spending 2\",\"price\":20.00}]");
	}
	
	@Test
	public void validate_that_when_we_get_total_spendings_then_the_content_is_the_sum_of_our_spendings_created() throws Exception {
		given().that_i_have_the_following_spendings(
				new Spending("Spending 1", BigDecimal.valueOf(10)),
				new Spending("Spending 2", BigDecimal.valueOf(20)))
		.when().i_post_a_new_spending("{\"description\": \"Spending 1\", \"price\":\"10\"}")
		.and().i_post_a_new_spending("{\"description\": \"Spending 2\", \"price\":\"20\"}")
		.and().i_get_total_spendings("/budgets/total")
		.then().the_status_is(HttpStatus.OK)
		.and().the_content_contains("\"spendings\":30.00");
	}
	
	@Test
	public void validate_that_an_spending_can_not_be_zero_and_will_return_a_bad_request() throws Exception {
		given().that_i_have_the_following_spendings(
				new Spending("Spending 1", BigDecimal.valueOf(0)))
		.when().i_post_a_new_spending("{\"description\": \"Spending 1\", \"price\":\"0\"}")
		.then().the_status_is(HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void validate_that_when_i_get_total_spendings_without_any_then_no_spendings_is_returned() throws Exception {
		when().i_get_total_spendings("/budgets/total")
		.then().the_status_is(HttpStatus.OK)
		.and().the_content_contains("\"spendings\":0.0");
	}

}
