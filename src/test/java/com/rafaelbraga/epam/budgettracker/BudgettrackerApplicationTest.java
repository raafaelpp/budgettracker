package com.rafaelbraga.epam.budgettracker;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.ConstraintViolation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.rafaelbraga.epam.budgettracker.models.Budget;
import com.rafaelbraga.epam.budgettracker.repositories.BudgetRepository;
import com.rafaelbraga.epam.budgettracker.services.BudgetService;

@RunWith(MockitoJUnitRunner.class)
public class BudgettrackerApplicationTest {

	@Spy
	@InjectMocks
	private BudgetService budgetService;

	@Mock
	private BudgetRepository budgetRepository;
	
	private Validator validator;
	
	@Before
	public void before() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void whenNewBudgetIsBeingSavedInBudgetService_shouldReturnIllegalArgumentException() {
		Budget budget = new Budget(1L, "Banana", BigDecimal.valueOf(2));
		
		budgetService.save(budget);

		Mockito.verify(budgetRepository, Mockito.times(1)).save(budget);
	}

	@Test
	public void whenValueOfZeroIsBeingSavedOnBudgetService_shouldReturnIllegalArgumentException() {
		Budget budget = new Budget(1L, "Banana", BigDecimal.valueOf(0));

		Mockito.lenient().doReturn(Optional.empty()).when(budgetRepository).findById(1L);

		budgetService.save(budget);
		
		Set<ConstraintViolation<Budget>> violations = validator.validate(budget);
		assertTrue(!violations.isEmpty());
	}

	@Test
	public void givenIGetTotalBudgetsThenResponseReturnsWithTotalSpendings() throws Exception {
		Mockito.doReturn(BigDecimal.valueOf(10)).when(budgetService).getAllSpendingsSum();

		BigDecimal spendings = budgetService.getAllSpendingsSum();

		Assert.assertEquals(spendings, BigDecimal.valueOf(10));
	}

}
