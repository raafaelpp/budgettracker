package com.rafaelbraga.epam.budgettracker.pojo;

import java.math.BigDecimal;

import com.tngtech.jgiven.annotation.Quoted;

public class Spending {

	@Quoted
	String spending;
	
	BigDecimal price;

	public Spending(String spending, BigDecimal price) {
		this.spending = spending;
		this.price = price;
	}

}
