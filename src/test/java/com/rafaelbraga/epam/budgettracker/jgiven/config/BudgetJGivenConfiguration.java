package com.rafaelbraga.epam.budgettracker.jgiven.config;

import org.springframework.http.HttpStatus;

import com.tngtech.jgiven.config.AbstractJGivenConfiguration;

public class BudgetJGivenConfiguration extends AbstractJGivenConfiguration {

	@Override
	public void configure() {
		setFormatter(HttpStatus.class, new HttpStatusFormatter());
	}

}
